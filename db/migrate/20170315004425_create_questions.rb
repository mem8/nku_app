class CreateQuestions < ActiveRecord::Migration[5.0]
  def change
    create_table :questions do |t|
      t.text :content
      t.integer :question_id
      t.references :user, foreign_key: true
      t.integer :is_flagged
      
      t.timestamps
    end
  end
end
