# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170414184744) do

  create_table "answers", force: :cascade do |t|
    t.text    "content"
    t.integer "rep",         default: 0
    t.integer "is_flagged"
    t.integer "user_id"
    t.integer "question_id"
    t.index ["question_id"], name: "index_answers_on_question_id"
    t.index ["user_id"], name: "index_answers_on_user_id"
  end

  create_table "course2users", force: :cascade do |t|
    t.integer "course_id"
    t.integer "user_id"
    t.index ["course_id"], name: "index_course2users_on_course_id"
    t.index ["user_id"], name: "index_course2users_on_user_id"
  end

  create_table "courses", force: :cascade do |t|
    t.string "name"
    t.string "academic_label"
    t.string "description"
  end

  create_table "questions", force: :cascade do |t|
    t.text    "title"
    t.text    "content"
    t.integer "rep",        default: 0
    t.integer "is_flagged"
    t.integer "course_id"
    t.integer "user_id"
    t.index ["course_id"], name: "index_questions_on_course_id"
    t.index ["user_id"], name: "index_questions_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string  "name"
    t.string  "email"
    t.integer "rep",         default: 0
    t.integer "accesslevel"
  end

end
