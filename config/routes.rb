Rails.application.routes.draw do
  #root 'courses#index' #old root
  root 'sessions#new' #new root -- for login page
  
  get '/login',   to: 'sessions#new'
  post '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'
  
  get '/moderator', to: 'control_panels#moderator'
  get '/registrar', to: 'control_panels#registrar'
  
  resources :course2users do
    
  end
  
  resources :courses do
    
  end
  resources :answers do
    member {get 'flag'}
    member {get 'unflag'}
    member {get 'upvote'}
    member {get 'downvote'}
  end
  resources :questions do
    member {get 'flag'}
    member {get 'unflag'}
    member {get 'upvote'}
    member {get 'downvote'}
  end
  resources :users do
    
  end
  
  
  #resources :questions, :shallow => true do 
  #  resources :answers
  #end
  
end
