class AnswersController < ApplicationController
  before_action :set_answer, only: [:show, :edit, :update, :destroy]

  # GET /answers
  # GET /answers.json
  def index
    @answers = Answer.all
  end

  # GET /answers/1
  # GET /answers/1.json
  def show
  end

  # GET /answers/new
  def new
    @answer = Answer.new
  end

  # GET /answers/1/edit
  def edit
  end
  
  def upvote
    @answer = Answer.find(params[:id])
    @answer.increment!(:rep)
    @answer.user.increment!(:rep)
    respond_to do |format|
      format.html { redirect_to @answer.question, notice: 'Answer was voted up!' }
      format.json { render :show, status: :ok, location: @answer.question }
    end
  end
  
  def downvote
    @answer = Answer.find(params[:id])
    @answer.decrement!(:rep)
    @answer.user.decrement!(:rep)
    respond_to do |format|
      format.html { redirect_to @answer.question, notice: 'Answer was voted down!' }
      format.json { render :show, status: :ok, location: @answer.question }
    end
  end
  
  #
  def flag
    @answer = Answer.find(params[:id])
    @answer.update(is_flagged: 1)
    respond_to do |format|
      format.html { redirect_to @answer.question, notice: 'Answer was flagged.' }
      format.json { render :show, status: :ok, location: @answer.question }
    end
  end
  
  #  
  def unflag
    @answer = Answer.find(params[:id])
    @answer.update(is_flagged: 0)
    respond_to do |format|
      format.html { redirect_to moderator_path, notice: 'Answer was unflagged.' }
      format.json { render :show, status: :ok, location: @answer.question }
    end
  end

  # POST /answers
  # POST /answers.json
  def create
    @answer = Answer.new(answer_params)
    @answer.question_id = get_question?
    @answer.user_id = get_user?

    respond_to do |format|
      if @answer.save
        format.html { redirect_to @answer.question, notice: 'Answer was successfully created.' }
        format.json { render :show, status: :created, location: @answer }
      else
        format.html { render :new }
        format.json { render json: @answer.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /answers/1
  # PATCH/PUT /answers/1.json
  def update
    respond_to do |format|
      if @answer.update(answer_params)
        format.html { redirect_to @answer.question, notice: 'Answer was successfully updated.' }
        format.json { render :show, status: :ok, location: @answer }
      else
        format.html { render :edit }
        format.json { render json: @answer.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /answers/1
  # DELETE /answers/1.json
  def destroy
    @answer.destroy
    respond_to do |format|
      format.html { redirect_to moderator_path, notice: 'Answer was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_answer
      @answer = Answer.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def answer_params
      params.require(:answer).permit(:content, :user_id, :question_id)
    end
end
