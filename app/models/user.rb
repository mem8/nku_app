class User < ApplicationRecord
    has_many :questions
    has_many :answers
    has_many :courses, :through => :course2users
    has_many :course2users
    accepts_nested_attributes_for :course2users
end
